// -- set up ---------
var express      = require('express');
var app          = express();
var port         = process.env.PORT || 8080;
var flash        = require('connect-flash');
var path         = require('path');
var logger       = require('morgan');
var createError  = require('http-errors')
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var router       = express.Router();
var config       = require('./config/config.js');
var mapConfig    = require('./config/mapConfig');
var firebase     = require('firebase');
var auth         = require('firebase/auth');
var database     = require('firebase/database');
var dotenv       = require('dotenv')
var fs = require('fs');
var index = require ('./routes/index');
var GoogleMapsAPI =require('googlemaps');

//setup jade for templating
app.set('views' , path.join(__dirname , 'views'));
app.set('view engine' , 'jade');

app.use(logger('dev'));
app.use(cookieParser());
// app.use(bodyParser());
//app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));
app.use(express.static(path.join(__dirname , 'public')));
app.use(router);
app.use(index);

// Dotenv  library to load our enviroment variables from the .env
dotenv.load();

// Initialize firebase:
firebase.initializeApp(config);


// Catch the 404 and forward to the error handler :rs
//app.use(function (req , res , next){
//    next(createError(404));
//});

// error handler:
app.use(function (err , req , res , next){
    // set locals , only providing error in development
    res.locals.messages = err.messages;
    res.locals.error = req.app.get('env') === 'development' ? err:{}

    // Debuggin the error messages in the place

    if (app.get('env') === 'development') console.log(err.stack);

    // Render error page :
    res.status(err.status || 500 );
    res.render('layout',{message: " Sorry there was some error "});

});

// launch the application ======================================
app.listen(port , (err) => {
    if (err)
        console.log(err.message);

    console.log('The magic happens on port ' + port);

});

module.exports = app;