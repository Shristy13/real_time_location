'use strict'
var express = require('express');
var router = express.Router();
var path = require('path');
const enviromentController = require(path.resolve( "controllers", "./enviroment.js" ));

router.get('/enviroment',enviromentController.list);
router.get('/enviroment/:id', enviromentController.getById);
router.post('/enviroment', enviromentController.add);

module.exports = router;
