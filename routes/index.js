//
var express = require('express');

// Frontend routes:
var login = require('./login');
var register = require('./register');
var gservice = require('./gservice');
var dashboard = require('./dashboard');
var testEnviroment = require('./enviroment');
var router = express.Router();

// backend route variables:
var enviroment = require('./api');

router.use('/login',login);
router.use('/register', register);
router.use('/gservice', gservice);
router.use('/dashboard', dashboard);
router.use('/enviroment',testEnviroment)
router.use('/api', enviroment)


router.get('/', function(req, res) {
   res.render('layout' , {message : "Hello! Welcome to I am here testing tool application :)"});
});

module.exports = router;