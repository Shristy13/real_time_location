'use strict'
var express = require('express');
var router = express.Router();
var firebase = require('firebase');

router.get('/' , function (req , res , next){
    console.log("I am  in the register page .");
     res.render('register');
});

// Create a user with email and password in firbase:
router.post('/',(req ,res) => {
    if(!req.body.email) {
            return res.status(401).send({ "message": "An `email` is required" });
        } else if(!req.body.password) {
            return res.status(401).send({ "message": "A `password` is required" });
        }
        var email = req.body.email;
        var password = req.body.password ;

        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function(user){
        console.log("Successfully created user account with uid:" , user.uid);
        })
        .catch(function(error) {
        console.log("error");
          // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
                  if (errorCode == 'auth/weak-password') {
                    res.send('The password is too weak.');
                  } else {
                  res.send(errorMessage);
                  }
                  console.log(error);
                  // [END_EXCLUDE]
                });
        });

module.exports = router;