'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('TestEnviroments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Applications: {
        type: Sequelize.STRING
      },
      Versions: {
        type: Sequelize.STRING
      },
      Deployments: {
        type: Sequelize.STRING
      },
      Dates: {
        type: Sequelize.DATE
      },
      Team: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TestEnviroments');
  }
};