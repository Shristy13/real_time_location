'use strict';
module.exports = (sequelize, DataTypes) => {
  const Enviroments = sequelize.define('Enviroments', {
    name: DataTypes.STRING,
    freezed: DataTypes.BOOLEAN
  }, {});
  Enviroments.associate = function(models) {
    // associations can be defined here
  };
  return Enviroments;
};