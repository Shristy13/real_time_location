'use strict';
module.exports = (sequelize, DataTypes) => {
  const TestEnviroment = sequelize.define('TestEnviroment', {
    Applications: DataTypes.STRING,
    Versions: DataTypes.STRING,
    Deployments: DataTypes.STRING,
    Dates: DataTypes.DATE,
    Team: DataTypes.STRING
  }, {});
  TestEnviroment.associate = function(models) {
    // associations can be defined here
  };
  return TestEnviroment;
};